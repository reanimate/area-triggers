using UnityEngine;
using System.Collections;

public class SphereMover : MonoBehaviour {
	public float radius = 10.0f;
	private float inc = 0.0f;
	// Use this for initialization
	void Start () {
		inc = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		inc += 0.025f;
		if(inc > Mathf.PI * 2)
			inc -= Mathf.PI * 2;
		this.transform.position = new Vector3(Mathf.Sin(inc) * radius,5.0f,Mathf.Cos(inc) * radius);	}
}
